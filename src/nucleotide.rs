use std::string::ToString;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Nucleotide {
    A,
    C,
    G,
    T,
}

impl ToString for Nucleotide {
    fn to_string(&self) -> String {
        String::from(match self {
            Nucleotide::A => "A",
            Nucleotide::C => "C",
            Nucleotide::G => "G",
            Nucleotide::T => "T",
        })
    }
}
