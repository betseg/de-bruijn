use core::fmt;

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Binary {
    F,
    T,
}

impl ToString for Binary {
    fn to_string(&self) -> String {
        String::from(match self {
            Binary::F => "0",
            Binary::T => "1",
        })
    }
}

impl fmt::Debug for Binary {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("{}", self.to_string()))
    }
}
