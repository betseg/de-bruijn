use de_bruijn::DeBruijn;
use petgraph::dot::Dot;

fn main() {
    // use de_bruijn::nucleotide::{Nucleotide, Nucleotide::*};
    // let mut g = DeBruijn::<Nucleotide, 7>::from_slice(&[A, T, G, G, A, A, G, T, C, G, C, G]);
    // g.extend_with_slice(&[G, A, G, G, A, A, G, T, C, C, T, T]);

    use de_bruijn::binary::Binary;
    let g = DeBruijn::<Binary, 3>::binary();

    // let g = DeBruijn::<_, 3>::full_graph(&["a", "b", "c"]);

    println!("{:?}", Dot::new(&*g));
}
