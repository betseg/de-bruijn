use crate::binary::Binary;
use crate::node_weight::NodeWeight;
use itertools::Itertools;
use petgraph::graph::Graph;
use std::{
    convert::TryInto,
    ops::{Deref, DerefMut},
};

#[derive(Default)]
pub struct DeBruijn<N, const NL: usize>(Graph<NodeWeight<N, NL>, N>);

impl<N, const NL: usize> DeBruijn<N, NL> {
    pub fn new() -> Self {
        Self(Default::default())
    }
}

impl<N, const NL: usize> DeBruijn<N, NL>
where
    N: Copy + std::cmp::PartialEq + std::fmt::Debug,
{
    pub fn from_slice(seq: &[N]) -> Self {
        let mut g = Self::new();
        g.extend_with_slice(seq);
        g
    }

    pub fn extend_with_slice(&mut self, seq: &[N]) {
        for weight in seq.windows(NL) {
            if !self.node_indices().any(|i| self[i].0 == weight) {
                self.add_node(NodeWeight::new(weight.try_into().unwrap()));
            }
        }
        self.create_edges();
    }

    pub fn full_graph(symbols: &[N]) -> Self {
        let mut g = Self::new();
        for weight in std::iter::repeat(symbols)
            .take(NL)
            .multi_cartesian_product()
        {
            let weight = weight.into_iter().copied().collect::<Vec<N>>();
            g.add_node(NodeWeight::new(&weight.try_into().unwrap()));
        }
        g.create_edges();
        g
    }

    fn create_edges(&mut self) {
        let ixs = self.node_indices().collect::<Vec<_>>();
        for tuple in std::iter::repeat(ixs).take(NL).multi_cartesian_product() {
            if !self.contains_edge(tuple[0], tuple[1])
                && self[tuple[0]][1..] == self[tuple[1]][..NL - 1]
            {
                self.0
                    .add_edge(tuple[0], tuple[1], self.0[tuple[1]][NL - 1]);
            }
        }
    }
}

impl<const NL: usize> DeBruijn<Binary, NL> {
    pub fn binary() -> Self {
        Self::full_graph(&[Binary::F, Binary::T])
    }
}

impl<N, const NL: usize> Deref for DeBruijn<N, NL> {
    type Target = Graph<NodeWeight<N, NL>, N>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<N, const NL: usize> DerefMut for DeBruijn<N, NL> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
