use std::{
    fmt,
    ops::{Deref, DerefMut},
};

pub struct NodeWeight<T, const N: usize>(pub(crate) [T; N]);

impl<T, const N: usize> NodeWeight<T, N>
where
    T: Copy,
{
    pub fn new(weight: &[T; N]) -> Self {
        Self(*weight)
    }
}

impl<T, const N: usize> fmt::Debug for NodeWeight<T, N>
where
    T: ToString,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(&self.to_string())
    }
}

impl<T, const N: usize> ToString for NodeWeight<T, N>
where
    T: ToString,
{
    fn to_string(&self) -> String {
        let mut s = String::new();
        for item in self.0.iter() {
            s.push_str(&item.to_string());
        }
        s
    }
}

impl<T, const N: usize> Deref for NodeWeight<T, N> {
    type Target = [T; N];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T, const N: usize> DerefMut for NodeWeight<T, N> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
